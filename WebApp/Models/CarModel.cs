﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class CarModel
    {
        [DisplayName("Id number")]
        public int Id { get; set; }

        [DisplayName("Model")]
        public string Name { get; set; }

        [DisplayName("Power")]
        public int Power { get; set; }

        [DisplayName("Cost to Customer")]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [DisplayName("What you get...")]
        public string Description { get; set; }
    }
}
