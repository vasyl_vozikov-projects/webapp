﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class ProductModelDTO
    {
        [DisplayName("Id number")]
        public int Id { get; set; }

        [DisplayName("Product Name")]
        public string Name { get; set; }

        [DisplayName("Cost to Customer")]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [DisplayName("What you get...")]
        public string Description { get; set; }

        public string PriceString { get; set; }
        public string ShortDescription { get; set; }
        public decimal Tax { get; set; }
        public ProductModelDTO(int id, string name, decimal price, string description)
        {
            Id = id;
            Name = name;
            Price = price;
            Description = description;

            PriceString = string.Format("{0:C}", price);
            ShortDescription = description.Length <= 25 ? description : description.Substring(0, 25);
            Tax = price * 0.08M;
        }
        public ProductModelDTO(ProductModel product)
        {
            Id = product.Id;
            Name = product.Name;
            Price = product.Price;
            Description = product.Description;

            PriceString = string.Format("{0:C}", product.Price);
            ShortDescription = product.Description.Length <= 25 ? product.Description : product.Description.Substring(0, 25);
            Tax = product.Price * 0.08M;
        }
    }
}
