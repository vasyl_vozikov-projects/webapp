﻿using System.Collections.Generic;
using WebApp.Models;

namespace WebApp.Services
{
    public interface ICarDataService
    {
        List<CarModel> GetAllProducts();
        List<CarModel> SearchProducts(string searchTerm);
        CarModel GetProductById(int id);
        int Insert(CarModel car);
        int Delete(CarModel car);
        int Update(CarModel car);
    }
}
