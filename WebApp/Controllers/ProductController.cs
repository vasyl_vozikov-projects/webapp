﻿using Bogus;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class ProductController : Controller
    {
        ProductsDAO repository;
        public ProductController()
        {
            repository = new ProductsDAO();
        }
        public IActionResult Index()
        {
            return View(repository.GetAllProducts());
        }
        public IActionResult ShowDetails(int id)
        {            
            return View(repository.GetProductById(id));
        }
        public IActionResult Create()
        {
            return View();
        }
        public IActionResult ProcessCreate(ProductModel product)
        {
            repository.Insert(product);
            return View("Index", repository.GetAllProducts());
        }
        public IActionResult Edit(int id)
        {
            return View("Edit", repository.GetProductById(id));
        }
        public IActionResult ProcessEdit(ProductModel product)
        {
            repository.Update(product);
            return View("Index", repository.GetAllProducts());
        }
        public IActionResult ProcessEditReturnPartial(ProductModel product)
        {
            repository.Update(product);
            return PartialView("_productCard", product);
        }
        public IActionResult ShowOneProductJson(int Id)
        {
            return Json(repository.GetProductById(Id));
        }
        public IActionResult Delete(int id)
        {
            ProductModel product = repository.GetProductById(id);
            repository.Delete(product);
            return View("Index", repository.GetAllProducts());
        }
        public IActionResult SearchForm()
        {
            return View();
        }
        public IActionResult SearchResults(string searchTerm)
        {
            return View("index", repository.SearchProducts(searchTerm));
        }
        public IActionResult Message()
        {
            return View();
        }
        public IActionResult Welcome(string name, int number = 13)
        {
            ViewBag.Name = name;
            ViewBag.Number = number;
            return View();
        }
    }
}
