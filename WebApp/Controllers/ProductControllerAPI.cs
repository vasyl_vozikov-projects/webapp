﻿using Bogus;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Description;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    [ApiController]
    [Route("api/")]
    public class ProductControllerAPI : Controller
    {
        ProductsDAO repository;
        public ProductControllerAPI()
        {
            repository = new ProductsDAO();
        }

        [HttpGet]
        [ResponseType(typeof (List<ProductModelDTO>))]
        public IEnumerable<ProductModelDTO> Index()
        {
            List<ProductModel> products = repository.GetAllProducts();
            //List<ProductModelDTO> productsDTO = new List<ProductModelDTO>();

            //foreach (ProductModel product in products)
            //{
            //    productsDTO.Add(new ProductModelDTO(product));
            //}

            IEnumerable<ProductModelDTO> productsDTO = from product in products
                                                  select new ProductModelDTO(product);
            return productsDTO;
        }

        [HttpPost("create")]
        public ActionResult<int> Create(ProductModel product)
        {
            int newId = repository.Insert(product);
            return newId;
        }

        [HttpPut("edit")]
        public ActionResult<ProductModel> ProcessEdit(ProductModel product)
        {
            repository.Update(product);
            return product;
        }

        [HttpGet("showoneproduct/{id}")]
        public ActionResult<ProductModelDTO> ShowOneProduct(int Id)
        {
            ProductModel product = repository.GetProductById(Id);
            ProductModelDTO productDTO = new ProductModelDTO(product);
            return productDTO;
        }

        [HttpDelete("delete/{id}")]
        public ActionResult<string> Delete(int id)
        {
            ProductModel product = repository.GetProductById(id);
            ProductModelDTO productDTO = new ProductModelDTO(product);
            repository.DeleteDTO(productDTO);

            string success = "success";
            return success;
        }

        [HttpGet("searchresults/{searchTerm}")]
        [ResponseType(typeof(List<ProductModelDTO>))]
        public IEnumerable<ProductModelDTO> SearchResults(string searchTerm)
        {
            List<ProductModel> products = repository.SearchProducts(searchTerm);
            IEnumerable<ProductModelDTO> productsDTO = from product in products
                                                       select new ProductModelDTO(product);
            return productsDTO;
        }
    }
}