﻿using Microsoft.AspNetCore.Mvc;
using System;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class CarController : Controller
    {
        public IActionResult Index()
        {
            HardCodedSampleCars hardCodedSampleCars = new HardCodedSampleCars();
            return View(hardCodedSampleCars.GetAllProducts());
        }
    }
}
