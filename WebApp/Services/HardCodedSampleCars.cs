﻿using Bogus;
using System.Collections.Generic;
using WebApp.Models;

namespace WebApp.Services
{
    public class HardCodedSampleCars : ICarDataService
    {
        static List<CarModel> carList = new List<CarModel>();
        public int Delete(CarModel car)
        {
            throw new System.NotImplementedException();
        }

        public List<CarModel> GetAllProducts()
        {
            if (carList.Count == 0)
            {
                for (int i = 0; i < 100; i++)
                {
                    carList.Add(new Faker<CarModel>()
                        .RuleFor(p => p.Id, i + 5)
                        .RuleFor(p => p.Name, f => f.Commerce.ProductName())
                        .RuleFor(p => p.Power, f => f.Random.Int(100, 999))
                        .RuleFor(p => p.Price, f => f.Random.Decimal(100))
                        .RuleFor(p => p.Description, f => f.Rant.Review())
                        );
                }                
            }
            return carList;
        }

        public CarModel GetProductById(int id)
        {
            throw new System.NotImplementedException();
        }

        public int Insert(CarModel car)
        {
            throw new System.NotImplementedException();
        }

        public List<CarModel> SearchProducts(string searchTerm)
        {
            throw new System.NotImplementedException();
        }

        public int Update(CarModel car)
        {
            throw new System.NotImplementedException();
        }
    }
}
