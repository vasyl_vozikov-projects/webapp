﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class AppointmentModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2)]
        [DisplayName("Patient's full name")]
        public string PatientName { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [DisplayName("Phone")]
        public int PatientPhoneNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string PatientEmailAddress{ get; set; }

        [Required]
        [DisplayName("Street")]
        public string PatientStreet { get; set; }

        [Required]
        [DisplayName("City")]
        public string PatientCity{ get; set; }

        [Required]
        [DataType(DataType.PostalCode)]
        [DisplayName("ZIP Code")]
        public int PatientZipCode { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Appointment Request Date")]
        public DateTime dateTime { get; set; }

        [DataType(DataType.Currency)]
        [DisplayName("Patient's Approximate Net Worth")]
        public decimal PatientNetWorth { get; set; }

        [DisplayName("Doctor's Last Name")]
        public string DoctorName { get; set; }

        [Range(1, 10)]
        [DisplayName("Patient's Preceived Level of Pain (1 low to 10 high)")]
        public int PainLevel { get; set; }
    }
}
