﻿using System.Collections.Generic;
using System.Linq;
using WebApp.Models;

namespace WebApp.Services
{
    public class SecurityService
    {
        UsersDAO usersDAO = new UsersDAO();
        public SecurityService()
        {
        }
        public bool IsValid(UserModel userModel)
        {
            return usersDAO.FindUserByNameAndPassword(userModel);
        }
    }
}
